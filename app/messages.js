const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {
    res.send(db.getItem())
});

router.get('/:id', (req, res) => {
    res.send(`A single product by ID will be here!`)
});

router.post('/', (req, res) => {
    db.addItem(req.body);
    res.send({messame:"OK"})
});

module.exports = router;